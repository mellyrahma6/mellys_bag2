 import 'package:flutter/material.dart';

class Warna {
  static Color green = Color.fromARGB(255, 69, 170, 74);
  static Color grey = Color.fromARGB(255, 242, 242, 242);
  static Color grey200 = Color.fromARGB(200, 242, 242, 242);
  static Color orenMuda = Color(0xffe99e1e);
  static Color biruKetuaan = Color(0xff14639e);
  static Color biruMuda = Color(0xff2da5d9);
  static Color merah = Color(0xffec1d27);
  static Color ijoMuda = Color(0xff8dc53e);
  static Color merahKeterangan = Color(0xfff43f24);
  static Color ijoKeterangan = Color(0xff72d2a2);
  static Color grey100 = Color(0xffa6a6a6);
  static Color ijoTua = Color(0xff0b945e);
  static Color biruKeterangan = Color(0xff68a9e3);
  static Color orenAgakDalem = Color(0xffe86f16);
}
